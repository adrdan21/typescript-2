import { client } from "../config/db.ts";
import { UsuarioDTO } from "../dto/usuario.ts";

export class UsuariosModel {
    async listar() {
        const showAll = await client.execute('select * from usuarios');

        return showAll.rows;
    }

    async registrar(usuario: UsuarioDTO) {
        await client.execute(`INSERT INTO usuarios(nombre, apellido, celular, email, psw)
            values(?, ?, ?, ?, ?)`,[
            usuario.nombre,
            usuario.apellido,
            usuario.celular,
            usuario.email,
            usuario.psw
        ]);
    }

    async actualizar_usuario(usuario:UsuarioDTO,id:number){
        await client.execute(`update usuarios set nombre=?,apellido=?,celular=?,email=?,psw=? WHERE id=?`,[
            usuario.nombre,
            usuario.apellido,
            usuario.celular,
            usuario.email,
            usuario.psw,
            id])            
}
    async eliminar_usuario(id:number){
        await client.execute(`delete from usuarios WHERE id=?`,[id])
    }

}