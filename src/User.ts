export class User{
    private nombre:string;
    private apellido:string;
    private celular:string;
    private email:string;
    private psw:string;

    constructor(nombre:string,apellido:string,celular:string,email:string,psw:string){
        this.nombre=nombre;
        this.apellido=apellido;
        this.celular=celular;
        this.email=email;
        this.psw=psw;
    }
    public getNombre():string{
        return this.nombre;
    }
    public getApellido():string{
        return this.apellido;
    }
    public getCelular():string{
        return this.celular;
    }
    public getEmail():string{
        return this.email;
    }
    public getContrasenia():string{
        return this.psw;
    }
}