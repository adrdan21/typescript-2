//Taller TypeScript
//Adrian Galindres & Jhonatan Lopez
//Facultad de Ingenieria y Ciencias Basicas
//Ingenieria de Sistemas
//Electiva Profesional II
import { client } from "./config/db.ts";
import { dataUser } from "./DataUtils.ts";
import { UsuariosModel } from './models/user_model.ts';
import { UsuarioController } from './controllers/Users.ts';


const usuariosModel = new UsuariosModel();
const usuarioController = new UsuarioController();

let opcSel;
while (opcSel != 5) {

    console.log("Taller TypeScript");
    console.log("1. Listar Usuarios");
    console.log("2. Insertar Usuarios");
    console.log("3. Editar Usuarios");
    console.log("4. Eliminar Usuarios");
    console.log( "5. Salir");

    const opcSel = parseInt(prompt("Seleccionar Opc") as string)


    switch (opcSel) {
        case 1: { 
            const usuarios = await usuarioController.listar();
            console.log(usuarios);
            break;
        }
        case 2: { 
            await usuarioController.registrar();
            break;
        }     
        case 3: {
            const usuarios = await usuarioController.listar();
            console.log(usuarios);
            const opcion = parseInt(prompt("opcion: ") as string)
            console.log("Asigna nuevos valores al usuario");
            await usuarioController.actualizar_usuario(opcion)
            break;
        }   
        case 4: {
            const usuarios = await usuarioController.listar();
            console.log(usuarios);
            const opcion = parseInt(prompt("opcion: ") as string);
            await usuarioController.eliminar_usuario(opcion)
            break;
        }
    }
}













