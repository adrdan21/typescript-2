export interface UsuarioDTO{
    nombre: string,
    apellido: string,
    celular: string,
    email: string,
    psw: string
}