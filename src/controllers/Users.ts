import { UsuariosModel } from '../models/user_model.ts';
import {UsuarioDTO} from '../dto/usuario.ts';
import { dataUser } from "../DataUtils.ts";

const usuariosModel = new UsuariosModel();
export class UsuarioController {
    async listar(){
        const usuarios = await usuariosModel.listar();
        console.log(usuarios);
    }

    async registrar(){

        const user = dataUser();
        await usuariosModel.registrar({
            nombre: user.getNombre(),
            apellido: user.getApellido(),
            celular: user.getCelular(),
            email: user.getEmail(),
            psw: user.getContrasenia(),
    })
    }
    async actualizar_usuario(id:number){
        const user2 = dataUser();
                const update = await usuariosModel.actualizar_usuario({
                nombre:user2.getNombre(), 
                apellido:user2.getApellido(),
                celular:user2.getCelular(),
                email: user2.getEmail(),
                psw: user2.getContrasenia(),},
                id)
    }
    async eliminar_usuario(id:number){
        const deleteRegister = await usuariosModel.eliminar_usuario(id);
        
    }
}